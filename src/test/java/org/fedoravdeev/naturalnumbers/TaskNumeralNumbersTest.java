package org.fedoravdeev.naturalnumbers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskNumeralNumbersTest extends NaturalNumeric {

    private TaskNumeralNumbers task;

    @Before
    public void init(){
        task = new TaskNumeralNumbers();
    }

    @Test
    public void getDescriptionPositiveTest() {
        String expected = "Natural Numbers\n" +
                "Output a comma separated row of Natural Numbers length 'n' and minimal pow of which is not less than 'm'.\n" +
                "Input parameters : length 'n' and value of minimal pow 'm'\n" +
                "Output: row of Natural Number\n";
        Assert.assertEquals(expected, task.getDescription());
    }

    @Test
    public void getDescriptionNegativeTest() {
        String expected = "";
        Assert.assertNotEquals(expected, task.getDescription());
    }

    @Test
    public void getListArgPositiveTest() {
        String expected = "length,minimalPow";
        Assert.assertEquals(expected, task.getListArg());
    }

    @Test
    public void getListArgNegativeTest() {
        String expected = "";
        Assert.assertNotEquals(expected, task.getListArg());
    }
}