package org.fedoravdeev.naturalnumbers;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class ConsoleParameterTest extends NaturalNumeric {

    @Test
    public void getValueByKeyLengthTest() {
        Integer expected = 5;
        ConsoleParameter consoleParameter = Mockito.mock(ConsoleParameter.class);
        when(consoleParameter.getValueByKey("length")).thenReturn(5);
        Assert.assertEquals(expected,consoleParameter.getValueByKey("length"));
    }

    @Test
    public void getValueByKeyMinimaplPow() {
        Integer expected = 99;
        ConsoleParameter consoleParameter = Mockito.mock(ConsoleParameter.class);
        when(consoleParameter.getValueByKey("minimalPow")).thenReturn(99);
        Assert.assertEquals(expected,consoleParameter.getValueByKey("minimalPow"));
    }

    @Test
    public void isKeyNegative() {
        ConsoleParameter consoleParameter = Mockito.mock(ConsoleParameter.class);
        when(consoleParameter.isKey("noLength")).thenReturn(false);
        Assert.assertFalse(consoleParameter.isKey("noLength"));
    }

    @Test
    public void isConsolePositive() {
        ConsoleParameter consoleParameter = Mockito.mock(ConsoleParameter.class);
        when(consoleParameter.isConsole()).thenReturn(true);
        Assert.assertTrue(consoleParameter.isConsole());
    }
}