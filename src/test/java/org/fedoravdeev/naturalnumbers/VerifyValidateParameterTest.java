package org.fedoravdeev.naturalnumbers;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class VerifyValidateParameterTest extends NaturalNumeric {

    @Test
    public void isValidateIOptionPositiveTest() {
        IOption option = Mockito.mock(IOption.class);
        Assert.assertTrue(VerifyValidateParameter.isValidate(option));
    }

    @Test
    public void isValidateIOption100ElementsNegativeTest() {
        IOption option = Mockito.mock(IOption.class);
        when(option.getValueByKey("length")).thenReturn(101);
        Assert.assertFalse(VerifyValidateParameter.isValidate(option));
    }

    @Test
    public void isValidateIOption99ElementsPositiveTest() {
        IOption option = Mockito.mock(IOption.class);
        when(option.getValueByKey("length")).thenReturn(99);
        Assert.assertTrue(VerifyValidateParameter.isValidate(option));
    }
    @Test
    public void isValidateIOptionMinimalPowNegativeTest() {
        IOption option = Mockito.mock(IOption.class);
        when(option.getValueByKey("length")).thenReturn(1);
        when(option.getValueByKey("minimalPow")).thenReturn(2147483647);
        Assert.assertFalse(VerifyValidateParameter.isValidate(option));
    }

    @Test
    public void isVerifyIOptionPositiveTest() {
        IOption option = Mockito.mock(IOption.class);
        when(option.isKey("length")).thenReturn(true);
        when(option.isKey("minimalPow")).thenReturn(true);
        Assert.assertTrue(VerifyValidateParameter.isVerify(option));
    }

    @Test(expected = IllegalArgumentException.class)
    public void isVerifyIOptionNegativeTest() throws IllegalArgumentException {
        IOption option = Mockito.mock(IOption.class);
        when(VerifyValidateParameter.isVerify(option)).thenThrow(new IllegalArgumentException());
    }
}