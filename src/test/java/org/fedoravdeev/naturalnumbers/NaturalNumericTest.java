package org.fedoravdeev.naturalnumbers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

public class NaturalNumericTest extends NaturalNumeric {

    @Test
    @DisplayName("should be compare Natural Numeric row ")
    public void compareResultNaturalNumericTest() {
        String expected = "10,11,12,13,14";
        NaturalNumeric naturalNumeric = new NaturalNumeric(5, 99);
        naturalNumeric.buildRowNaturalNumber();
        Assert.assertEquals(expected, naturalNumeric.toString());
    }

    @Test
    @DisplayName("should be compare object of Natural Numerics ")
    public void compare2ObjectsNaturalNumericTest() {
        NaturalNumeric naturalNumeric1 = new NaturalNumeric(5, 99);
        naturalNumeric1.buildRowNaturalNumber();
        NaturalNumeric naturalNumeric2 = new NaturalNumeric(5, 99);
        naturalNumeric2.buildRowNaturalNumber();
        Assert.assertEquals(naturalNumeric1, naturalNumeric2);
    }
}