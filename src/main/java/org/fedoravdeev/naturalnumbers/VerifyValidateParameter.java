package org.fedoravdeev.naturalnumbers;

public final class VerifyValidateParameter {

    public static boolean isValidate(IOption opt) {
        TaskNumeralNumbers task = new TaskNumeralNumbers();
        String listArg = task.getListArg();
        String[] arrayOfArg = listArg.split(",");

        if (arrayOfArg.length == task.getCountListArg()) {
            if (opt.getValueByKey(arrayOfArg[0]) > 100) {
                System.out.println("List of Natural Numbers not more 100 elements.");
                return false;
            }
            if (opt.getValueByKey("minimalPow") >= Integer.MAX_VALUE ) {
                System.out.println("Integer MAX_VALUE !");
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isVerify(IOption opt) throws IllegalArgumentException {
        String listArg = new TaskNumeralNumbers().getListArg();
        String[] arrayOfArg = listArg.split(",");
        for (String key : arrayOfArg) {
            if (!opt.isKey(key)) {
                throw new IllegalArgumentException("Illegal option name '" + key + "'");
            }
        }
        return true;
    }
}
