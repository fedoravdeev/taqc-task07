package org.fedoravdeev.naturalnumbers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ConsoleParameter implements IOption {

    private HashMap<String, Integer> mapParam = new HashMap<>();
    private boolean isConsole;

    public ConsoleParameter() {
        isConsole = true;
        //setConsoleParams();
    }

    public boolean setConsoleParams() {
        boolean res = true;
        String listArg = new TaskNumeralNumbers().getListArg();
        String[] listParam = listArg.split(",");
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(streamReader);

        try {
            System.out.print("Input "+listParam[0]+":");
            mapParam.put(listParam[0], Integer.parseInt(bufferedReader.readLine()));
            System.out.print("Input " + listParam[1]+ ":");
            mapParam.put(listParam[1], Integer.parseInt(bufferedReader.readLine()));
        } catch (NumberFormatException e) {
            System.out.println("Please input correct value");
            res = false;
        } catch (IOException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    @Override
    public boolean hasNext() {
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader bufferedReader = new BufferedReader(streamReader);

        System.out.print("\nWould you like continue: y/n (yes/no)");
        try {
            String answer = bufferedReader.readLine();
            System.out.printf("Your answer: %s \n\n", answer);
            return answer.equalsIgnoreCase("y") || answer.equalsIgnoreCase("yes");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean setParam() {
        return setConsoleParams();
    }

    @Override
    public Integer getValueByKey(String key) {
        return mapParam.get(key);
    }

    @Override
    public boolean isKey(String key) {
        return mapParam.containsKey(key);
    }

    @Override
    public boolean isConsole() {
        return isConsole;
    }

}
