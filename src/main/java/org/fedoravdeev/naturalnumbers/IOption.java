package org.fedoravdeev.naturalnumbers;

public interface IOption {
    Integer getValueByKey(String key);
    boolean isKey(String key);
    boolean isConsole();
    boolean hasNext();
    boolean setParam();
}
