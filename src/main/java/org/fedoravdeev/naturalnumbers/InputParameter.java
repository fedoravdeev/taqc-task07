package org.fedoravdeev.naturalnumbers;

import java.util.HashMap;

public class InputParameter implements IOption {

    private HashMap<String, Integer> mapParam = new HashMap<>();
    private boolean isConsole;

    public InputParameter(String[] args) {
        isConsole = false;
        String listArg = new TaskNumeralNumbers().getListArg();
        String[] listParam = listArg.split(",");
        for (int i = 0; i < listParam.length; i++) {
            try {
                mapParam.put(listParam[i], Integer.parseInt(args[i]));
            } catch (NumberFormatException nfe) {
                System.out.println("Please input correct value '" + listParam[i] + "'.");
            }
        }
    }

    @Override
    public Integer getValueByKey(String key) {
        return mapParam.get(key);
    }

    @Override
    public boolean isKey(String key) {
        return mapParam.containsKey(key);
    }

    @Override
    public boolean isConsole() {
        return isConsole;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public boolean setParam() {
        return false;
    }
}
