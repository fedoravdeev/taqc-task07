package org.fedoravdeev.naturalnumbers;

abstract class Task {
    abstract public String getDescription();
    abstract public String getListArg();
}
