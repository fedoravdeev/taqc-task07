package org.fedoravdeev.naturalnumbers;

public class TaskNumeralNumbers extends Task{

    private String listArg;
    private String description;
    private int countListArg;

    public TaskNumeralNumbers() {
        listArg = "length,minimalPow";
        description = "Natural Numbers\n" +
                "Output a comma separated row of Natural Numbers length 'n' and minimal pow of which is not less than 'm'.\n" +
                "Input parameters : length 'n' and value of minimal pow 'm'\n" +
                "Output: row of Natural Number\n";
        countListArg = listArg.split(",").length;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getListArg() {
        return listArg;
    }

    public int getCountListArg() {
        return countListArg;
    }
}
