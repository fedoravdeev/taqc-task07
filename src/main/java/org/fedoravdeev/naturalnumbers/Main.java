package org.fedoravdeev.naturalnumbers;

import static org.fedoravdeev.naturalnumbers.VerifyValidateParameter.isVerify;
import static org.fedoravdeev.naturalnumbers.VerifyValidateParameter.isValidate;

public class Main {

    public static void main(String[] args) {

        TaskNumeralNumbers task = new TaskNumeralNumbers();
        System.out.println(task.getDescription());

        IOption inputOption;
        if (args.length > 0) {
            inputOption = new InputParameter(args);
        } else {
            inputOption = new ConsoleParameter();
        }

        do {
            if (inputOption.isConsole()) {
                inputOption.setParam();
            }
            if (!isVerify(inputOption)) break;
            if (!isValidate(inputOption)) break;

            NaturalNumeric naturalNumeric;
            naturalNumeric = new NaturalNumeric(inputOption);
            naturalNumeric.buildRowNaturalNumber();
            System.out.println(naturalNumeric.toString());

        } while (inputOption.hasNext() && inputOption.isConsole());

    }

}
