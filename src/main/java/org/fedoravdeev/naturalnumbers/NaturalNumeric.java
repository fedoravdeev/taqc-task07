package org.fedoravdeev.naturalnumbers;

import java.util.Arrays;

public class NaturalNumeric {

    private static final String SEPARATOR = ",";
    private static final int NUMBER_LIST_ARG = 2;
    private int length;
    private int minimalPow;
    private Integer[] arrNaturalNumeric;

    public NaturalNumeric() {
        minimalPow = 0;
        length = 0;
        arrNaturalNumeric = new Integer[0];
    }

    public NaturalNumeric(int length, int minimalPow) {
        this.length = length;
        this.minimalPow = minimalPow;
        arrNaturalNumeric = new Integer[this.length];
    }

    public NaturalNumeric(IOption inputParameter) {
        this.length = inputParameter.getValueByKey("length");
        this.minimalPow = inputParameter.getValueByKey("minimalPow");
        arrNaturalNumeric = new Integer[this.length];
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setMinimalPow(int minimalPow) {
        this.minimalPow = minimalPow;
    }
    public int getLength() {
        return length;
    }

    public int getMinimalPow() {
        return minimalPow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NaturalNumeric that = (NaturalNumeric) o;
        return Arrays.equals(arrNaturalNumeric, that.arrNaturalNumeric);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arrNaturalNumeric);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < getLength(); i++) {
            result.append(arrNaturalNumeric[i]) ;
            if (i < getLength() - 1) {
                result.append(SEPARATOR);
            }
        }
        return result.toString();
    }

    public void buildRowNaturalNumber() {
        Integer minimalPow = getMinimalPow();
        for (int i = 0, j = 0; j < getLength(); i++) {
            if (i * i > minimalPow) {
                arrNaturalNumeric[j] = i;
                j++;
            }
        }
    }

    public void setParam(IOption inputParameter) {
        TaskNumeralNumbers task = new TaskNumeralNumbers();
        String[] listArgs = task.getListArg().split(",");
        if(listArgs.length == NUMBER_LIST_ARG){
            setLength(inputParameter.getValueByKey(listArgs[0]));
            setMinimalPow(inputParameter.getValueByKey(listArgs[1]));
        }
    }
}

